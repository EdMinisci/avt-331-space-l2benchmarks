function [Fobj,C,CEQ]=ENTRY_Fobj_sym(xa,AoA,ll,FL)
C=[];
CEQ=[];
Fobj=[];

global DDD
nAoA=0;
x=xa.*(ll(2,:)-ll(1,:))+ll(1,:);

% Call FOSTRAD for each AoA, write results to matrix EQ
for AoA=AoA.min:AoA.step:AoA.max
    AoA
    nAoA=nAoA+1;
    eQ=ENTRY_WriteRead_sym(x,AoA,FL); % Interface to FOSTRAD
    CL(nAoA)=eQ(1);
    CD(nAoA)=eQ(2);
    LIFT(nAoA)=eQ(3);
    DRAG(nAoA)=eQ(4);
    HF(nAoA)=eQ(5);
    EQ(nAoA,1)=eQ(1);
    EQ(nAoA,2)=eQ(2);
    EQ(nAoA,3)=eQ(3);
end

C=max(HF(:))-550000;  % HF >= HF0 (HF0: max heat flow of the original configuration)
C=[C; -LIFT(1)+1500]; % Lift >= Lift0 (Lift0: lift of the original configuration)

% Reading craft and bay points

craft=stlread("craft.00001.stl");
P=csvread("BayPoints");
bay=stlread("largeelipse.stl");

% Ensuring craft is of correct format
if isfield(craft,'vertices')
    Craft=craft;
else
    Craft.faces=craft.ConnectivityList;
    Craft.vertices=craft.Points;
end

% Ensuring bay is of correct format
if isfield(bay,'vertices')
    Bay=bay;
else
    Bay.faces=bay.ConnectivityList;
    Bay.vertices=bay.Points;
end

%% Computing Exposed Bay Volume

% Determining which points are inside the craft, output is a lengthP logical

intersect=intriangulation(Craft.vertices,Craft.faces,P);
vertintersect=intriangulation(Craft.vertices,Craft.faces,Bay.vertices);
sz=size(intersect);
outer=0;
vertouter=0;

for i=1:sz
    if intersect(i)==false
        outer=outer+1;
    end
end

percent(1)=(1000*outer)/sz(1);
sz=size(vertintersect);

for i=1:sz
    if vertintersect(i)==false
        vertouter=vertouter+1;
    end
end

percent(2)=(1000*vertouter)/sz(1);

C=[C; percent'];


constrout=EQ;
constrout=[constrout percent];

writematrix(constrout,'constr.csv','WriteMode','append')

Fobj=max(EQ(:,1)./EQ(:,2));
DDD=[DDD; xa, x, Fobj EQ C' CEQ'];
DDDp=DDD;
save ./RESULTS/DDDp DDDp
