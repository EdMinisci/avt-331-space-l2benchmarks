function Opt_ENTRY_symmetry()
%% Needed to run MIMMO on my system ... maybe not needed in other systems
unix('cd /home/kqb12101/Documents/local/MATLAB/R2021b/sys/os/glnxa64/; rm -f libstdc++.so.6; ln -s libstdc++.so.6.0.28 libstdc++.so.6')
pwd
% ------------------------------------------------------------------------------

close all
clc
format long g
global DDD
DDD=[];

addpath('./MIMMO')

% In the preprocession script the bay and the initial position of the nodes
% are initalised
[M,ismirrored]=PREprocession();

%% Angle Of Attack Parameters (one single ange is used for this case)

AoA.min=-10;
AoA.max=-10;
AoA.step=-10;
save AoA AoA

%% Optimisation Initialisation

xLast = [];
myf = [];
myc = [];
myceq = [];

%Forming upper and lower bounds

bound=0.25;
i=1;
j=0;
while ismirrored(i,1)<1
    j=j+1;
    ll(1,j)=-bound;
    ll(2,j)=bound;
    if abs(M(i,2))>1e-3
        j=j+1;
        ll(1,j)=-bound;
        ll(2,j)=bound;
    end
    j=j+1;
    ll(1,j)=-bound;
    ll(2,j)=bound;
    i=i+1;
end
D=size(ll,2);
% Applying Lower and Upper Bounds
lb=zeros(1,D);
ub=ones(1,D);

%Fidelity level
FL=1;

%Create objective + constraint functions
fitnessfcn.obj    = @(x)objfun(x,AoA,ll,FL);
fitnessfcn.constr = @(x)constr(x,AoA,ll,FL);

% Create optimisation options
FMCoptions=optimset('Display','iter-detailed','DiffMinChange',5e-2,'Algorithm','sqp');

%% Local Optimisation
tic
x0d=zeros(1,D);
x0=(x0d-ll(1,:))./(ll(2,:)-ll(1,:));

% Evaluation of objective funtion and constraints of the original
% configuration
[ff,c,cceq] = ENTRY_Fobj_sym(x0,AoA,ll,FL);
disp('Objective funtion value of the original configuration')
disp(ff)
disp('Inequality constraints of the original configuration')
disp(c)
disp('Equality constraints of the original configuration')
disp(cceq)

% fmincon optimisation
simtime=toc;
iterguess=500; %423 at 8 points
duration_min=simtime*iterguess;
duration_max=7.5*iterguess;


close all

n_Opt=1; % global counter for the number of model evaluations

[x,fval,exitflag,output,lambda,grad,hessian] = fmincon(fitnessfcn.obj, x0,[],[],[],[],lb,ub,fitnessfcn.constr,FMCoptions);
% diary off
    function obj = objfun(u_opt, AoA, ll, FL)
        if ~isequal(u_opt,xLast) % Check if computation is necessary
            n_Opt=n_Opt+1
            [myf,myc,myceq] = ENTRY_Fobj_sym(u_opt,AoA,ll,FL);
            xLast = u_opt;
        end
        obj = myf;
    end

    function [c, ceq] = constr(u_opt, AoA, ll, FL)
        if ~isequal(u_opt,xLast) % Check if computation is necessary
            [myf,myc,myceq] = ENTRY_Fobj_sym(u_opt,AoA,ll,FL);
            n_Opt=n_Opt+1
            xLast = u_opt;
        end
        c = myc;
        ceq = myceq;
    end

% Results
save ./RESULTS/ENTRY_solutions_sqp_FL3 x fval exitflag output
%% Back to the matlab library - Needed to run MIMMO on my system ... maybe not needed in other systems
unix('cd /home/kqb12101/Documents/local/MATLAB/R2021b/sys/os/glnxa64/; rm -f libstdc++.so.6; ln -s libstdc++.so.6.0.25 libstdc++.so.6')
pwd
% -------------------------------------------------------------------------

end

