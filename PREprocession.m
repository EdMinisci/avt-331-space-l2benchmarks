function [M,ismirrored]=PREprocession()
addpath('./MIMMO')

%% Bay Point Grid Generation

%% Bay Point Grid Generation

bay=stlread("largeelipse.stl");
% Step size for sampling point grid
step=0.05;
% To ensure bay is in correct format (bay.faces and bay.vertices)
if isfield(bay,'vertices')
    Bay=bay;
else
    Bay.faces=bay.ConnectivityList;
    Bay.vertices=bay.Points;
end

% Find Bounds
xbmin=min(Bay.vertices(:,1));
xbmax=max(Bay.vertices(:,1));
ybmin=min(Bay.vertices(:,2));
ybmax=max(Bay.vertices(:,2));
zbmin=min(Bay.vertices(:,3));
zbmax=max(Bay.vertices(:,3));

% "Equivalent Cylinder" dimensions, not currently used
h=max([xbmax-xbmin,ybmax-ybmin,zbmax-zbmin]);
D=min([xbmax-xbmin,ybmax-ybmin,zbmax-zbmin]);

% Grid Generation
X=xbmin:step:xbmax+step;
Y=ybmin:step:ybmax+step;
Z=zbmin:step:zbmax+step;

P(1,1)=X(1);
P(1,2)=Y(1);
P(1,3)=Z(1);
for i=1:length(X)
    for j=1:length(Z)
        for k=1:length(Y)
        P(size(P,1)+1,1)=X(i);
        P(size(P,1),2)=Y(k);
        P(size(P,1),3)=Z(j);
        end
    end
end
disp('Gridding Done!')

% Determining which points are inside the bay, output is a lengthP logical
nocull=intriangulation(Bay.vertices,Bay.faces,P);
disp('Insiding Done!')

% Culling all points outside bay
n_cull=1;
kill=0;
while n_cull<=size(P,1)
      kill=kill+1;
    if nocull(n_cull)<1
       P(n_cull,:)=[];
       nocull(n_cull)=[];
    else
       n_cull=n_cull+1;
    end
    if kill>100000
        disp('Error: While Loop iterated over 10^5')
        break
    end
end
disp('Culling Done!')
% Displaying final point matrix, disabled currently
% scatterMatrix(P)

% Writing to .csv
csvwrite("BayPoints",P);

%% Node Generation

% Full craft exceeds memory of this device
craft=stlread("craft.lofi.stl");

% Distance between planes of intersection
nplanes=3;

% Number of nodes per plane, to work correctly it must be odd
nnodes=3;

% Manual points, currently disabled
% oldM=csvread("MIMMO/INPUT/manualpoints.csv");

% planarNodeGen inputs the craft, plane step and number or nodes, outputs matrix of nodes
oldM=planarNodeGen(craft,nplanes,nnodes);
%oldM=oldM(1:9,:);

M=oldM;
lengthM=length(M);
% Apply Symmetry
j=lengthM;
for i=1:lengthM
    if abs(M(i,2))>1e-3
        j=j+1;
        M(j,1)=M(i,1);
        M(j,2)=-M(i,2);
        M(j,3)=M(i,3);
        ismirrored(j,1)=1;
        ismirrored(j,2)=i;
    end
end
disp('Writing point matrix...')

% Mimmo input matrix
csvwrite("MIMMO/INPUT/point.csv",M);
% Index of which points are mirrored (column 1, logical) and which point
% they are mirroring (column 2, integer)
csvwrite("MIMMO/INPUT/ismirrored.csv",ismirrored);

% Displaying final node matrix, disabled currently
scatter3(M(:,1),M(:,2),M(:,3),20,'filled','black')