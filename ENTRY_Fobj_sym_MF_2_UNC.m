function [Fobj,C,CEQ]=ENTRY_Fobj_sym_MF_2_UNC(xa,AoA,ll,FL)
C=[];
CEQ=[];
Fobj=[];

global DDD
nAoA=0;
x=xa.*(ll(2,:)-ll(1,:))+ll(1,:);
if FL==2
    VRF2 =[ 0.00321723491710009        0.0246416004231526       -0.0383555975008983       0.00125382458368524       -0.0420617187972852 ...
           0.0239746821210762       -0.0101469514839949       -0.0416489939893272        0.0346208228806366        0.0310062900944072  ...
           0.0432881383748929         0.047646561407381       0.00664344372700417       -0.0157984346576346        -0.043224970949775];
    x=x+VRF2;
elseif FL==3
    VRF3 =[ -0.00898586023826049        0.0371530758091383           0.0370587476697       -0.0346008182687336        0.0370889829764928 ...
           -0.0129642889909056        0.0344416482263835     -0.000579739124289588     0.00100983685145032       -0.0218848050428867 ...
           -0.01687345070406        0.0214755389038213        0.0159143804353757       -0.0267864043413872       -0.0491874862803726];
    x=x+VRF3;
end

% Call FOSTRAD for each AoA, write results to matrix EQ
for AoA=AoA.min:AoA.step:AoA.max
    AoA
    nAoA=nAoA+1;
    eQ=ENTRY_WriteRead_sym(x,AoA,FL); % Interface to FOSTRAD
    CL(nAoA)=eQ(1);
    CD(nAoA)=eQ(2);
    LIFT(nAoA)=eQ(3);
    DRAG(nAoA)=eQ(4);
    HF(nAoA)=eQ(5);
    EQ(nAoA,1)=eQ(1);
    EQ(nAoA,2)=eQ(2);
    EQ(nAoA,3)=eQ(3);
end

C=(max(HF(:))-550000)/550000;  % HF >= HF0 (HF0: max heat flow of the original configuration)
C=[C; (-LIFT(1)+1500)/1500]; % Lift >= Lift0 (Lift0: lift of the original configuration)



% Reading craft and bay points

craft=stlread("craft.00001.stl");
P=csvread("BayPoints");
bay=stlread("largeelipse.stl");

% Ensuring craft is of correct format
if isfield(craft,'vertices')
    Craft=craft;
else
    Craft.faces=craft.ConnectivityList;
    Craft.vertices=craft.Points;
end

% Ensuring bay is of correct format
if isfield(bay,'vertices')
    Bay=bay;
else
    Bay.faces=bay.ConnectivityList;
    Bay.vertices=bay.Points;
end

%% Computing Exposed Bay Volume

% Determining which points are inside the craft, output is a lengthP logical

intersect=intriangulation(Craft.vertices,Craft.faces,P);
vertintersect=intriangulation(Craft.vertices,Craft.faces,Bay.vertices);
sz=size(intersect);
outer=0;
vertouter=0;

for i=1:sz
    if intersect(i)==false
        outer=outer+1;
    end
end

percent(1)=(1000*outer)/sz(1);
sz=size(vertintersect);

for i=1:sz
    if vertintersect(i)==false
        vertouter=vertouter+1;
    end
end

percent(2)=(1000*vertouter)/sz(1);

C=[C; percent'];

Fobj=max(EQ(:,1)./EQ(:,2));
DDD=[DDD; xa, x, Fobj EQ C' CEQ'];

DDDp=DDD;
save ./RESULTS/DDDp DDDp
