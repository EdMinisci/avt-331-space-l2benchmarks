# L2 Space problems
L2 Space problems interface the low-fidelity aerodynamic and aerothermidynamic module of FOSTRAD with the morpher MIMMO.
A description of of FOSTRAD and MIMMO (with instructions and tips to install it) follow.
All the auxiliary functions are in the zip file, which should be decompressed "here"


# FOSTRAD

(The Free Open Source Tool for Re-entry of Asteroids and Debris)

`FOSTRAD` is a simulation suite that allows for the estimation of aerodynamics and aerothermodynamics of an object experiencing flow-fields during atmospheric re-entry scenarios. It has better prediction capability than object-oriented re-entry codes as it employs local panel formulation to compute aerodynamic and aerothermodynamic coefficients. The unique capabilities of the code are described below.

#### 1. Grid Refinement
An internal mesh refiner is used to ensure that there are enough facets to provide accurate aerodynamic and aerothermodynamic coefficients.

#### 2. Visible Facet Detection
The visibility facet detection algorithm employs a fast and robust formulation based on occlusion culling and back-face culling. This approach gives the capability to detect the shadowed facets based on object orientation and self-shadowing of facets.

#### 3. Local radius Computation
A local radius estimation algorithm is implemented to provide better and consistent heat-flux computations.

#### 4. Shape-based Correction Factor
Area-ratio based drag and pitching moment correction factors are applied for cylindrical and parallelepiped geometries based on DSMC simulations.

#### 5. Aerothermodynamic Models,
Stagnation point heat transfer in the continuum flow regime can be computed using four different semi-empirical models.
1. SCARAB model (`sc`) : Provides higher heat flux for TPS based spacecraft.
2. Detra-Kemp-Riddel model (`krd`): Uses a Reynolds number formulation with effective nose radius of the object and an assumption of super catalytic wall boundary condition.
3. Fay-Riddel model (`fr`): Includes dissociation effects with fully catalytic boundary conditions.
4. Van-Driest model (`vd`): Simplified model of Fay-Riddel formulation.

All the above models use a local inclination angle to compute overall heat transfer.

#### 6. Transitional Bridging Function
A local radius based bridging model as function of Knudsen number and effective nose radius is implemented. This model has been calibrated to DSMC results to improve accuracy.


Requirements
------

`FOSTRAD` is currently written in `MATLAB` and hence needs an active MATLAB license to use the source code. A "binary" stereolithography model (STL file) of the relevant geometry is also required.


Under development
--------
1. In the spirit of making the software accessible to everyone, an updated version in python language is under development.
2. Multi-body object definition.
3. Re-entry trajectory analysis suite.


Cite:
------
Please cite the following,
```
@inproceedings{mehta2015open,
  title={An open source hypersonic aerodynamic and aerothermodynamic modelling tool},
  author={Mehta, Piyush and Minisci, Edmondo and Vasile, Massimiliano and Walker, Andrew C and Brown, Melrose},
  booktitle={8th European Symposium on Aerothermodynamics for Space Vehicles},
  year={2015}
}
```
```
@inproceedings{falchi2017fostrad,
  title={FOSTRAD: An advanced open source tool for re-entry analysis},
  author={Falchi, Alessandro and Renato, Viola and Minisci, Edmondo and Vasile, Massimiliano},
  booktitle={15th Reinventing Space Conference},
  year={2017}
}
```

Disclaimer
------
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the MPL license for more details.

The copyright holders are not liable for any damage(s) incurred due to improper use of `FOSTRAD`.

##### Contact:
1. Prof.Massimiliano Vasile ("mx-the-gray") - massimiliano.vasile@strath.ac.uk
2. Dr.Edmondo Minisci ("edmondominisci") - edmondo.minisci@strath.ac.uk
3. Dr.Marco Fossati ("MarcoFossati") - marco.fossati@strath.ac.uk



# Installation of MIMMO/BITPIT

To dowload MIMMO and BITPIT, please refer to http://optimad.github.io/mimmo/

This guide assumes starting from a fresh linux ubuntu install (previously used 20.04 LTS)

## Prerequisites

The following required prerequisites can be installed via terminal, gcc gfortran, libxml2, openblas, cmake, git.

$ sudo apt update

$ sudo apt install gcc gfortran libxml2 libxml2-dev libopenblas-base libopenblas-dev cmake curses-cmake-gui git

Note: The original installation involved the downloading and installing of BLAS from Netlib prior to the installation of openBLAS, it is possible that openBLAS must be installed manually on new installations, instructions for this can be found at https://www.openblas.net/

Additional libraries

Additional developer libraries for BLAS, LAPACKE, CBLAS and LAPACKE are required, these can again be installed via terminal (again it is possible that these are no longer required due to openBLAS)

$ sudo apt install libatlas-dev liblapack-dev liblapacke-dev

### OpenMPI

OpenMPI should be downloaded from https://www.open-mpi.org/software/ompi/v4.1/ and the installation instructions on https://www.open-mpi.org/faq/?category=building should be followed. Essentially after extracting the .tar.gz the following steps should be performed in a shell.

$ cd openmpi-4.1.1
/openmpi-4.1.1$ ./configure --prefix=/usr/local

After which OpenMPI can be made.

/openmpi-4.1.1$ shell$ make all install


### PETSC

The quick-start instructions on https://petsc.org/release/install/ should be followed to install PETSC, they are as follows…

Perform compiler sanity checks

$ printf '#include<stdio.h>\nint main(){printf("cc OK!\\n");}' > t.c && cc t.c && ./a.out && rm -f t.c a.out
$ printf '#include<iostream>\nint main(){std::cout<<"c++ OK!"<<std::endl;}' > t.cpp && c++ t.cpp && ./a.out && rm -f t.cpp a.out
$ printf 'program t\nprint"(a)","gfortran OK!"\nend program' > t.f90 && gfortran t.f90 && ./a.out && rm -f t.f90 a.out

Clone PETSC repo from git

$ git clone -b release https://gitlab.com/petsc/petsc
$ cd petsc
/petsc$ git pull

It is important to run the configure with the argument “--with-shared-libraries”

/petsc$ ./configure –with-shared-libraries

Make PETSC

/petsc$ make all check

If all went well the following output should be visible...

Running test examples to verify correct installation
Using PETSC_DIR=/your/petsc/dir and PETSC_ARCH=your-petsc-arch
C/C++ example src/snes/examples/tutorials/ex19 run successfully with 1 MPI process
C/C++ example src/snes/examples/tutorials/ex19 run successfully with 2 MPI processes
Fortran example src/snes/examples/tutorials/ex5f run successfully with 1 MPI process
Completed test examples
After installing PETSC it is important to set both PETSC_DIR and PETSC_ARCH as global variables, this is done by adding 2 lines to bashrc. To find bashrc open the home directory in file explorer and ensure “Show hidden files”is checked. Then add the following lines at the bottom with a text editor…
export PETSC_DIR=/{PETSC DIRECTORY PATH}/petsc
export PETSC_ARCH="arch-linux-c-debug"
The PETSC_ARCH is the first folder within the PETSC directory, this is usually  arch-linux-c-debug.

## Bitpit

Bitpit can be downloaded at https://optimad.github.io/bitpit/downloads/ and the installation guide“INSTALL.md” contained within can be followed. 

Create a build directory

/bitpit-bitpit-1.7.0$ mkdir build
/bitpit-bitpit-1.7.0$ cd build

Run ccmake, the cmake gui

/bitpit-bitpit-1.7.0/build$ ccmake ../

Within cmake the following configuration options are available, the required choices are highlighted in bold.

 BUILD_DOCUMENTATION              ON                                           
 BUILD_EXAMPLES                   ON                                           
 BUILD_SHARED_LIBS                ON                                           
 CMAKE_BUILD_TYPE                 Release                                      
 CMAKE_INSTALL_PREFIX             /usr/local                                   
 DOC_EXTRACT_PRIVATE              OFF                                          
 ENABLE_MPI                       OFF                                          
 LTO_STRATEGY                     Auto                                         
 MAKE_EXECUTABLE                  /usr/bin/make                                
 PETSC_ARCH                       arch-linux-c-debug                           
 PETSC_CURRENT                    ON                                           
 PETSC_DIR                        /home/tommy/packages/petsc                   
 VERBOSE_MAKE                     OFF                                          

Make sure that PETSC_ARCH and PETSC_DIR show the values previously assigned in bashrc. Afterwards press c to configure, assign options and repeat until the option to press g to generate is presented. Once the makefile has been generated bitpit can be made.

/bitpit-bitpit-1.7.0/build$ make

Then to install…

/bitpit-bitpit-1.7.0/build$ sudo make install

Finally to test the installation…

/bitpit-bitpit-1.7.0/build$ cd test
/bitpit-bitpit-1.7.0/build/test$ ctest

## MIMMO

MIMMO has a very similar installation process to bitpit, it can be downloaded at https://optimad.github.io/mimmo/downloads/ and the installation guide“INSTALL.md”contained within can be followed. 

Create a build directory

/mimmo-mimmo-1.4.0$ mkdir build
/mimmo-mimmo-1.4.0$ cd build

Run ccmake, the cmake gui

/mimmo-mimmo-1.4.0/build$ ccmake ../

Within cmake the following configuration options are available, the required choices are highlighted in bold.

 BITPIT_DIR                       /usr/local/lib/cmake/bitpit-1.7              
 BUILD_DOCUMENTATION              ON                                           
 BUILD_EXAMPLES                   ON                                           
 BUILD_SHARED_LIBS                ON                                           
 BUILD_XMLTUI                     ON                                           
 CMAKE_BUILD_TYPE                 Release                                      
 CMAKE_INSTALL_PREFIX             /usr/local                                   
 DOC_EXTRACT_PRIVATE              OFF                                          
 ENABLE_MPI                       OFF                                          
 LTO_STRATEGY                     Auto                                         
 MAKE_EXECUTABLE                  /usr/bin/make                                
 MIMMO_MODULE_GEOHANDLERS         ON                                           
 MIMMO_MODULE_IOCGNS              OFF                                          
 MIMMO_MODULE_IOOFOAM             OFF                                          
 MIMMO_MODULE_PROPAGATORS         OFF                                          
 MIMMO_MODULE_UTILS               ON                                           
 PETSC_ARCH                       arch-linux-c-debug                           
 PETSC_CURRENT                    ON                                           
 PETSC_DIR                        /home/tommy/packages/petsc                   
 VERBOSE_MAKE                     OFF                                          

Again make sure that PETSC_ARCH and PETSC_DIR show the values previously assigned in bashrc. It is very important that MIMMO_MODULE_GEOHANDLERS and MIMMO_MODULE_UTILS are both on. Configure, generate and make as before.

/mimmo-mimmo-1.4.0/build$ make

Then to install…

/mimmo-mimmo-1.4.0/build$ sudo make install

Finally to test the installation…

/mimmo-mimmo-1.4.0/build$ cd test
/mimmo-mimmo-1.4.0/build/test$ ctest
