function [points]=planarNodeGen(craft,nplanes,pointsperplane)

% Ensuring craft is in correct format
if isfield(craft,'vertices')
    Craft=craft;
else
    Craft.faces=craft.ConnectivityList;
    Craft.vertices=craft.Points;
end
step_angular=deg2rad(360/pointsperplane);
n_select=0;
% Find Bounds
xmin=min(Craft.vertices(:,1));
xmax=max(Craft.vertices(:,1));
ymin=min(Craft.vertices(:,2));
ymax=max(Craft.vertices(:,2));
zmin=min(Craft.vertices(:,3));
zmax=max(Craft.vertices(:,3));

step_planes=(xmax-xmin)/(nplanes);

% Generate Plane
P(1,2)=1.1*ymax;
P(1,3)=1.1*zmin;
P(2,2)=1.1*ymin;
P(2,3)=1.1*zmin;
P(3,2)=1.1*ymin;
P(3,3)=1.1*zmax;
P(4,2)=1.1*ymax;
P(4,3)=1.1*zmax;
% Build Plane Along X
for i=0.95*xmin:step_planes:0.95*xmax

    P(1,1)=i;
    P(2,1)=i;
    P(3,1)=i;
    P(4,1)=i;
    clear IntPlane
    IntPlane.vertices=P;
    IntPlane.faces=[1,2,3;1,3,4];
    % Determining Intersection Surface
    [IntMat,IntSurf]=SurfaceIntersection(IntPlane,Craft);
    % Selecting Maximum Points, assumes roughly convex shape
    if length(IntSurf.vertices)>3
        for theta=0:step_angular:pi   
            for j=1:length(IntSurf.vertices)

                [pol(j,1), pol(j,2)]=cart2pol(IntSurf.vertices(j,2),IntSurf.vertices(j,3));
                pol(j,1)=pol(j,1)+theta;
                [cart(j,1),cart(j,2)]=pol2cart(pol(j,1),pol(j,2));
            end
            cartmax=max(cart(:,2));
            for j=1:length(IntSurf.vertices)
                if cart(j,2)>=cartmax
                   n_select=n_select+1;
                   points(n_select,1)=i;
                   points(n_select,2)=IntSurf.vertices(j,2);
                   points(n_select,3)=IntSurf.vertices(j,3);
                end
            end
%         scatter (cart(:,1),cart(:,2),'r')
%         hold on
%         scatter (IntSurf.vertices(:,2),IntSurf.vertices(:,3),'c')

        end

    else
        % This code will break for 0-thickness shapes and tangential planes
        n_select=n_select+1;
        points(n_select,:)=P(1,:);
        n_select=n_select+1;
        points(n_select,:)=P(2,:);
        n_select=n_select+1;
        points(n_select,:)=P(3,:);
    end
% Plots of planes, intersections and points, currently disabled
%     scatter3(IntSurf.vertices(:,1),IntSurf.vertices(:,2),IntSurf.vertices(:,3),20)
%     hold on
%     scatterMatrix(P)
%     IntPlane=triangulation([1,2,3;1,3,4],P);
%     hold on
%     trimesh(IntPlane)
%     trimesh(triangulation(Craft.faces,Craft.vertices),'FaceColor','None','EdgeColor','None')
end
% scatter3(points(:,1),points(:,2),points(:,3),10)
% hold on
% trimesh(triangulation(Craft.faces,Craft.vertices),'FaceColor','None','EdgeColor','None')
end